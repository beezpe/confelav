<?php 

    $posts = array(
        'prendas' => array(
            "post_id"=> 1,
            "post_url" => "blog/2021/requerimientos-prendas-sin-conocimiento-textil",
            "post_img_large" => "_articles/2021/requerimientos-prendas-sin-conocimiento-textil/large.jpg",
            "post_img_short" => "_articles/2021/requerimientos-prendas-sin-conocimiento-textil/short.jpg",
            "post_title" => "¿Tienes un requerimiento de prendas y no tienes un adecuado conocimiento textil? ",
            "post_resumen" => "En Confelav ya hemos vestido diferentes empresas y hemos colaborado en determinar sus líneas de blancos y colores. ",
            "post_category" => "variados",
            "post_date" => "04 / 01 / 2021"
        ),
        'lavanderia' => array(
            "post_id"=> 2,
            "post_url" => "blog/2021/elegir-lavanderia-industrial-para-prendas",
            "post_img_large" => "_articles/2021/elegir-lavanderia-industrial-para-prendas/large.jpg",
            "post_img_short" => "_articles/2021/elegir-lavanderia-industrial-para-prendas/short.jpg",
            "post_title" => "¿Cómo elegir la lavandería industrial adecuada para mis prendas?",
            "post_resumen" => "La especialización, experiencia y la innovación son determinantes al momento de elegir o cambiar al proveedor de lavandería industrial.",
            "post_category" => "variados",
            "post_date" => "05 / 01 / 2021"
        ),
        'mascarillas' => array(
            "post_id"=> 3,
            "post_url" => "blog/2021/mascarillas-de-tela-porque-usarlas",
            "post_img_large" => "_articles/2021/mascarillas-de-tela-porque-usarlas/large.jpg",
            "post_img_short" => "_articles/2021/mascarillas-de-tela-porque-usarlas/short.jpg",
            "post_title" => "Mascarillas de tela: ¿Por qué usarlas y cómo hacerlas?",
            "post_resumen" => "Las mascarillas de tela ayudan a frenar la propagación del virus ya que reducen el riesgo de exposición potencial de una persona infectada, tenga síntomas o no.",
            "post_category" => "variados",
            "post_date" => "06 / 01 / 2021"
        ),
        'requerimientos' => array(
            "post_id"=> 4,
            "post_url" => "blog/2021/cuanto-tiempo-duran-prendas",
            "post_img_large" => "_articles/2021/cuanto-tiempo-duran-prendas/large.jpg",
            "post_img_short" => "_articles/2021/cuanto-tiempo-duran-prendas/short.jpg",
            "post_title" => "¿Cuánto tiempo me duran las prendas?",
            "post_resumen" => "La principal preocupación de las empresas que compran prendas recae sobre la duración y la vida útil de las mismas",
            "post_category" => "variados",
            "post_date" => "07 / 01 / 2021"            
        )
    );


    
    
     
    return $posts;  

    //echo "{$posts['prendas']['titulo']}";

 

?>