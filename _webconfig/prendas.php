<?php 

	$prenda1 = "Prendas hospitalarias, enfermería y médica";
	$prenda1_img ="_private/img/prendas/prendas-hospitalarias.png";
	$prenda1_url ="prendas/hospitalarias";

	$prenda1_cat1 = "Médicos y enfermeras";
	$prenda1_cat1_img ="_private/img/escenarios/medicos-enfermeros.svg";

	$prenda1_cat2 = "SOP";
	$prenda1_cat2_img ="_private/img/escenarios/sop.svg";

	$prenda1_cat3 = "Hospitalaria";
	$prenda1_cat3_img ="_private/img/escenarios/hospitalaria.svg";

	$prenda1_cat4 = "Neo";
	$prenda1_cat4_img ="_private/img/escenarios/neo.png";

	$prenda2 = "Prendas hoteleras y housekeeping";
	$prenda2_img ="_private/img/prendas/prendas-hoteleras.png";
	$prenda2_url ="prendas/hoteleras";

	$prenda2_cat1 = "Hotelera";
	$prenda2_cat1_img ="_private/img/escenarios/hotel.png";
	$prenda2_cat2 = "Housekeeping";
	$prenda2_cat2_img ="_private/img/escenarios/housekeping.png";

	$prenda3 = "Prendas industriales";
	$prenda3_img ="_private/img/prendas/prendas-industriales.png";
	$prenda3_url ="prendas/industriales";
	$prenda3_cat1_img ="_private/img/escenarios/industrial.png";

	$prenda4 = "Prendas administrativas";
	$prenda4_img ="_private/img/prendas/prendas-administrativas.png";
	$prenda4_url ="prendas/administrativas";
	$prenda4_cat1_img ="_private/img/vectors/sala-administrativa.svg";

?>