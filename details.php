<?php 
    require '_webconfig/root.php';
    $page_title = "Confelav 🏅 Confecciones de Lavandería";
?>

<!DOCTYPE html>

<html lang="es">

<head>
    <?php include '_partials/head.php' ?>
</head>

<body>
    <!-- Header -->
    <?php include '_partials/header.php';?>
    <!-- /Header -->

    <main class="main-content richtext">
        <img class="blue-stripe" src="./img/vectors/fondo.svg" alt="Bg">

        <section class="section-details">
            <div class="contenT">
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner" style="background-image:url(./img/blog/limpioeluniforme.png);"></div><br><br>

                        <div class="content-Details">
                            <div>
                                <div type="button" class="tags">
                                    <img src="./img/icons/tag1.svg" alt="icon"> 
                                    Categoría
                                </div>
                                <div type="button" class="tags">
                                    <img src="./img/icons/tag1.svg" alt="icon"> 
                                    Categoría
                                </div>
                                <div type="button" class="tags">
                                    <img src="./img/icons/tag1.svg" alt="icon"> 
                                    Categoría
                                </div>
                                <div type="button" class="tags">
                                    <img src="./img/icons/tag1.svg" alt="icon"> 
                                    Categoría
                                </div>
                            </div>
                            <div class="contenido">
                                <p class="title">
                                    Título de la entrada
                                </p>
                                <p class="fecha">
                                    <img src="./img/icons/calendar.svg" alt="Fecha">
                                    Fecha: 01/01/2020
                                </p>

                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt quidem corrupti inventore, unde enim doloribus? Voluptatum vero quidem alias veritatis est ea maiores veniam laudantium voluptates, corporis dolore distinctio consectetur!</p>

                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque ratione voluptate sint nihil nemo nulla blanditiis doloribus cum, repellat eveniet. Unde aperiam temporibus nesciunt voluptatibus, modi distinctio. Magni, eveniet illum.</p>

                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus obcaecati exercitationem, dicta numquam natus ratione nostrum dolorem quibusdam facere porro cumque in aliquam sunt! Sunt, dolores tempora? Distinctio, recusandae repellat!</p>

                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Incidunt quidem corrupti inventore, unde enim doloribus? Voluptatum vero quidem alias veritatis est ea maiores veniam laudantium voluptates, corporis dolore distinctio consectetur!</p>

                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque ratione voluptate sint nihil nemo nulla blanditiis doloribus cum, repellat eveniet. Unde aperiam temporibus nesciunt voluptatibus, modi distinctio. Magni, eveniet illum.</p>

                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus obcaecati exercitationem, dicta numquam natus ratione nostrum dolorem quibusdam facere porro cumque in aliquam sunt! Sunt, dolores tempora? Distinctio, recusandae repellat!</p>
                            </div>
                        </div>
                        
                    </div>
                </div>

            </div>

        </section>
    </main>

     <!-- Footer -->
     <?php include '_partials/footer.php';?>
    <!-- /Footer -->

    <!-- Scripts -->
    <?php include '_partials/scripts.php';?>
    <!-- /Scripts -->

</body>


</html>