
<script src="<?php echo $host ?>_private/vendors/jquery/jquery.min.js"></script>
<script src="<?php echo $host ?>_private/vendors/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo $host ?>_private/vendors/owl/owl.min.js"></script>
<script src="<?php echo $host ?>_private/vendors/aos/aos.min.js"></script>
<script src="<?php echo $host ?>_private/vendors/isotope/isotope.pkgd.min.js"></script>
<script src="<?php echo $host ?>_private/js/app.js"></script>
<script src="<?php echo $host ?>_private/js/core/menu.js"></script>
