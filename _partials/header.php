<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTR29DS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->



<header class="header">
    <contenedor layout-max>
        <section class="header-container">
            <a class="header-container-logo" href="./">
                <img src="_private/img/logotipo/confelav-logo.svg" class="confelav-logo" alt="Logo">
            </a>
            <div class="header-options">
                <ul class="header-options-ul">
                    <li>
                        <a class="header-options-link" href="#prendas" id='prendasact'>Prendas</a>
                        <ul>
                            <li><a href="<?php echo $prenda1_url; ?>" class="submenu-link"><?php echo $prenda1; ?></a></li>
                            <li><a href="<?php echo $prenda2_url; ?>" class="submenu-link"><?php echo $prenda2; ?></a></li>
                            <li><a href="<?php echo $prenda3_url; ?>" class="submenu-link"><?php echo $prenda3; ?></a></li>
                            <li><a href="<?php echo $prenda4_url; ?>" class="submenu-link"><?php echo $prenda4; ?></a></li>
                        </ul>
                    </li>

                    <li><a class="header-options-link tm-menu-nosotros" href="#nosotros" id='nosotrosact'>¿Por qué elegirnos?</a></li>

                    <li><a class="header-options-link tm-menu-tendencias" href="#tendencias" id='tendenciasact'>Tendencias</a></li>
                    <li><a class="header-options-link tm-menu-experiencia" href="#experiencia">Experiencia</a></li>

                    <li><a class="header-options-link tm-menu-blog" href="confeblog">Blog </a></li>

                    <li><a class="header-options-link tm-menu-contacto" href="#contacto" id='contactoact'>Contáctanos</a></li>
                </ul>
                
                <a  class="menu-cellphone"  href="tel:+51 962927662">
                    <img class="whatsapp-content-icon" src="_private/img/socialmedia/icon-cellphone.svg" alt="WhatsApp" />
                </a>
                <div class="wvw">
                    <a target="_blank" class="whatsapp-content tm-menu-whatsapp" href="https://wa.me/+51962927662">
                        <img class="whatsapp-content-icon" src="_private/img/socialmedia/icon-whatsapp.svg" alt="WhatsApp" />
                        <span class="whatsapp-content-text">962927662</span>
                    </a>
                </div>
            </div>
        </section>
    </contenedor>
</header>






<menu menu-responsive>

    <button menu-button type="button">
        <menu-close></menu-close>
    </button>

    <menu-overlay></menu-overlay>

    <nav menu-nav role="navigation">
        <ul menu-ul>
            <li menu-li>
                <a menu-a href="#prendas">
                    <span menu-title>Prendas</span>
                </a>
            </li>
            <li menu-li>
                <a menu-a href="#nosotros" class="tm-menu-nosotros">
                    <span menu-title>¿Por qué elegirnos?</span>
                </a>
            </li>
            <li menu-li>
                <a menu-a href="#tendencias" class="tm-menu-tendencias">
                    <span menu-title>Tendencias</span>
                </a>
            </li>
            <li menu-li>
                <a menu-a href="#experiencia" class="tm-menu-experiencia">
                    <span menu-title>Experiencia</span>
                </a>
            </li>
            <li menu-li>
                <a menu-a href="#blog" class="tm-menu-blog">
                    <span menu-title>Blog</span>
                </a>
            </li>
            <li menu-li>
                <a menu-a href="#contacto" class="tm-menu-contacto">
                    <span menu-title>Contáctanos</span>
                </a>
            </li>
        </ul>
    </nav>
</menu>


