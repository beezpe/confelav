<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PTR29DS');</script>
<!-- End Google Tag Manager -->


<base href="<?php echo $host ?>">


<meta charset="utf-8" />

<meta name="encoding" charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="all, index, follow" />
<meta name="googlebot" content="all, index, follow" />
<meta http-equiv="cache-control" content="no-cache" />
<meta property="og:type" content="website" />
<meta name="twitter:card" content="summary_large_image" />
<meta property="og:locale" content="es_ES" />
<meta property="og:locale:alternate" content="es_ES" />
<meta http-equiv="content-language" content="es_ES" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="owner" content="Beez.pe" />
<meta name="author" content="Beez.pe" />
<meta name="publisher" content="Beez.pe" />
<meta name="copyright" content="Beez.pe" />
<meta name="twitter:creator" content="Beez.pe" />
<meta name="twitter:site" content="Beez.pe" />
<meta name="generator" content="Beez.pe" />
<meta name="organization" content="Beez.pe" />
<meta property="og:image:width" content="598" />
<meta property="og:image:height" content="274" />

<link rel="icon" sizes="192x192" href="_private/img/logotipo/favicon.png" />
<link rel="icon" sizes="32x32" href="_private/img/logotipo/favicon.png" />
<meta property="twitter:image" content="_private/img/logotipo/favicon.png" />
<meta property="og:image" content="_private/img/logotipo/favicon.png" />
<meta property="og:image:secure_url" content="_private/img/logotipo/favicon.png" />
<link rel="apple-touch-icon-precomposed" href="_private/img/logotipo/favicon.png" />
<meta name="msapplication-TileImage" content="_private/img/logotipo/favicon.png" />
<link rel="icon" type="image/jpg" href="_private/img/logotipo/favicon.png" />
<meta property="og:url" content="<?php echo $host ?>" />
<meta property="og:site_name" content="<?php echo $host ?>" />
<meta property="twitter:url" content="<?php echo $host ?>" />
<link rel="canonical" href="<?php echo $host ?>" />
<link rel="shortlink" href="<?php echo $host ?>" />
<link rel="dns-prefetch" href="//s.w.org" />
<link rel="dns-prefetch" href="//fonts.googleapis.com" />
<link href="https://fonts.gstatic.com" crossorigin rel="preconnect" />
<meta name="keywords" content="<?php echo $host_keywords ?>" />
<meta name="description" content="<?php echo $host_description ?>" />
<meta name="twitter:description" content="<?php echo $host_description ?>" />
<meta property="og:description" content="<?php echo $host_description ?>" />

<meta property="og:title" content="<?php echo $page_title ?>" />
<meta name="twitter:title" content="<?php echo $page_title ?>" />
<meta name="dcterms.title" content="<?php echo $page_title ?>" />
<meta name="DC.title" content="<?php echo $page_title ?>" />
<meta name="application-name" content="<?php echo $page_title ?>" />
<meta name="title" content="<?php echo $page_title ?>" />
<title><?php echo $page_title ?></title>



<link rel="stylesheet" href="_private/css/pages/core.css" />
<link rel="stylesheet" href="_private/vendors/bootstrap/bootstrap.min.css" media="all">

<link rel="stylesheet" href="_private/css/pages/site.css">

<link rel="stylesheet" href="_private/vendors/owl/owl.min.css">
<link rel="stylesheet" href="_private/vendors/owl/owl.theme.min.css">

<link rel="stylesheet" href="_private/vendors/animate/animate.min.css" />
<link rel="stylesheet" href="https://unpkg.com/aos@2.3.1/dist/aos.css" >

<script src="https://kit.fontawesome.com/7eb4686721.js" crossorigin="anonymous"></script>

<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Convergence&family=Exo+2&family=Wendy+One" rel="stylesheet">


