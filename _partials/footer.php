<section class="marcas fondo-hilo"></section>

<footer id="contacto">
    
    <section class="section-footer">
        <contenedor layout-max>
            <div class="row footer-canales">
                <div class="col-md-6 col-lg-3 col-sm-12 col-xs-12 footer-canal footer-columns--is-center">
                    <div class="footer-title">
                        <img src="_private/img/icons/telefono.svg" class="icon-footer" alt="Telefono">
                        <span class="title-footer">Teléfono</span>
                    </div>
                    <div class="footter-subtitle">
                        <a target="_blank" class="footer-description" href="https://wa.me/+51962927662">01-6361303</a>
                    </div>

                    <div class="footer-title m-t-20">
                        <img src="_private/img/socialmedia/mobile-phone.svg" class="icon-footer" alt="Celular">
                        <span class="title-footer">Celular</span>
                    </div>
                    <div class="footter-subtitle">
                        <a target="_blank" class="footer-description" href="https://wa.me/+51962927662">962 927 662</a>
                    </div>

                    
                </div>
                <div class="col-md-6 col-lg-3 col-sm-12 col-xs-12 footer-canal footer-columns--is-center">
                    <div class="footer-title">
                        <img src="_private/img/icons/correo.svg" class="icon-footer" alt="Correo"><span class="title-footer">
                        Correo</span>
                    </div>
                    
                    <div class="footter-subtitle">
                        <a class="footer-description pl-0" href="mailto:contacto@confelav.pe">contacto@confelav.pe</a>
                    </div>

                    <div class="footer-title m-t-20">
                        <img src="_private/img/icons/direccion.svg" class="icon-footer" alt="Direccion">
                        <span class="title-footer">Dirección</span>
                    </div>
                    <div class="footter-subtitle">
                        <span class="footer-description">
                            <div class="footer-columns--is-center">José M.</div>
                            <div class="footer-columns--is-center">Barrionuevo 219 - Chorrillos</div>
                        </span>
                    </div>

                </div>
                <div class="col-md-6 col-lg-3 col-sm-12 col-xs-12 footer-canal footer-columns--is-center border-none">
                    <div class="footer-title footer-title-map">
                        <img src="_private/img/icons/direccion.svg" class="icon-footer" alt="Direccion">
                        <span class="title-footer">Mapa de sitio</span>
                    </div>
                    <div class="footter-subtitle footer-columns">
                        <a class="footer-description" href="#prendas">Prendas</a>
                        <a class="footer-description" href="#nosotros">¿Por qué elegirnos?</a>
                        <a class="footer-description" href="#tendencias">Tendencias</a>
                        <a class="footer-description" href="#experiencia">Experiencia</a>
                        <a class="footer-description footer--is-blog" href="confeblog">Blog</a>
                        <a class="footer-description footer--is-contacto" href="#contacto">Contáctanos</a>
                    </div>
                </div>
                

                <div class="col-md-6 col-lg-3 col-sm-12 col-xs-12 footer-canal footer-columns--is-center border-l">
                    <div class="footer-title">
                        <img src="_private/img/icons/redes.svg" class="icon-footer" alt="Redes Sociales">

                        <span class="title-footer">Redes sociales</span>
                    </div>

                   
                    <ul class="icon-redes">
                        <li class="redes-footer">
                            <a href="https://wa.me/+51962927662" target="_blank" rel="noopener noreferrer"
                                class="">
                                <img src="_private/img/socialmedia/footer-fb.svg" class="" alt="Icono whatsapp">
                            </a>
                        </li>
                        <li class="redes-footer">
                            <a href="facebook.com/confelav.pe" target="_blank" rel="noopener noreferrer"
                                class="">
                                <img src="_private/img/socialmedia/footer-wsp.svg" width="26px" height="26px"
                                    class="" alt="Icono facebook">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="footer-logo">
                <a href="/" class=""><img src="_private/img/logotipo/logo-white.png"alt="Logo"></a>
                <p class="copyright">Copyright© Confelav.pe - 2020</p>
            </div>
            
            
            
        </contenedor>
    </section>
    
</footer>