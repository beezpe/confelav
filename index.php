<?php 
    include '_webconfig/root.php';
    include '_webconfig/prendas.php';
    include '_webconfig/slider.php';
    $page_title = "Confelav 🏅 Confecciones de Lavandería";
?>

<!DOCTYPE html>

<html lang="es" class="home">
    <head>
        <link href="https://fonts.googleapis.com/css2?family=Balsamiq+Sans:wght@700&display=swap" rel="stylesheet" />
        <?php include '_partials/head.php' ?>
    </head>

    <body>
        <!-- Header -->
        <?php include '_partials/header.php';?>
        <!-- /Header -->

        <section id="banner" class="sectionS banner-content">
            <contenedor layout-max layout-space>

                <div class="main-slider">
                    <img src="_private/img/fondo/fondo-circle-2.svg" alt="icon" class="imgSlide" />

                    <div class="main-slider-content">
                        <div class="owl-carousel owl-theme owl-carousel-banner test">
                        <div class="item" style="background-image: url(<?php echo $slider_img1; ?>);" data-dot='<div class="test-squart test-one"></div>'></div>
                        <div class="item" style="background-image: url(<?php echo $slider_img2; ?>);" data-dot='<div class="test-squart test-two"></div>'></div>
                        <div class="item" style="background-image: url(<?php echo $slider_img3; ?>);" data-dot='<div class="test-squart test-three"></div>'></div>
                        <div class="item" style="background-image: url(<?php echo $slider_img4; ?>);" data-dot='<div class="test-squart test-four"></div>'></div>
                    </div>

                    <div class="main-slider-buttons">
                        <img id="banner-left" src="_private/img/icons/icon-arrow.svg" class="slider-arrow slider-arrow-left" />
                        <img id="banner-right" src="_private/img/icons/icon-arrow.svg" class="slider-arrow slider-arrow-right" />
                    </div>
                    </div>
                </div>

                <main-text>

                    <h1 class="main-text" data-animation="animated zoomIn">
                        <div>
                            <span data-depth="0.30" data-animation="animated zoomIn">
                                Especialistas en <br />

                                <span class="ml11 azul">
                                  <span class="text-wrapper">
                                    <span class="line line1"></span>
                                    <span class="letters">confección de prendas</span>
                                  </span>
                                </span>

                                <br />
                                para uso y lavado industrial
                            </span>
                        </div>
                        
                    </h1>


                    

                    <div class="button-secondary-container">
                        <a href="#contacto" class="button button-secondary tm-home-cotizar">
                            <img src="_private/img/icons/telefono.svg" alt="icon" class="button-icon" />
                            Cotizar ahora
                        </a>
                        <span class="button-secondary-effect">
                            <span class="circle top-left"></span>
                            <span class="circle top-left"></span>
                            <span class="circle top-left"></span>

                            <span class="button effect-button"></span>

                            <span class="circle bottom-right"></span>
                            <span class="circle bottom-right"></span>
                            <span class="circle bottom-right"></span>
                        </span>

                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" class="button-secondary-svg">
                            <defs>
                            <filter id="goo">
                            <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur"></feGaussianBlur>
                            <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo"></feColorMatrix>
                            <feComposite in="SourceGraphic" in2="goo"></feComposite>
                            </filter>
                            </defs>
                        </svg>
                    </div>
                    
                </main-text>

                
            </contenedor>
        </section>

        <main class="main-content">
            <section id="prendas" class="prendas">
                <contenedor layout-max layout-space class="inner">
                    <div class="disable-mobile">
                        <div class="row">
                            <div class="col-lg-3">
                                <a href="<?php echo $prenda1_url; ?>" class="tm-home-hospitalaria">
                                    <div class="card-prendas">
                                        <div class="border-img-prenda">
                                            <img class="img-prendas" src="<?php echo $prenda1_img; ?>" />
                                        </div>
                                        <div class="w-100">
                                            <div class="sub-blue"></div>
                                        </div>
                                        <span class="text-prendas text-prendas-hospitales">
                                            <?php echo $prenda1; ?>
                                        </span>

                                        <div class="cta">
                                            <span class="mas"><img src="_private/img/icons/mas.svg" alt="Mas" /></span>

                                            <span class="ver">
                                                Ver prendas
                                                <img src="_private/img/icons/flecha.png" alt="Ver" />
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-lg-3">
                                <a href="<?php echo $prenda2_url; ?>" class="tm-home-hospitalaria">
                                    <div class="card-prendas">
                                        <div class="border-img-prenda">
                                            <img class="img-prendas" src="<?php echo $prenda2_img; ?>" />
                                        </div>
                                        <div class="w-100">
                                            <div class="sub-blue"></div>
                                        </div>
                                        <span class="text-prendas text-prendas-hotel font-text"><?php echo $prenda2; ?></span>

                                        <div class="cta">
                                            <span class="mas"><img src="_private/img/icons/mas.svg" alt="Mas" /></span>

                                            <span class="ver">
                                                Ver prendas
                                                <img src="_private/img/icons/flecha.png" alt="Ver" />
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-lg-3">
                                <a href="<?php echo $prenda3_url; ?>" class="tm-home-industriales">
                                    <div class="card-prendas">
                                        <div class="border-img-prenda">
                                            <img class="img-prendas" src="<?php echo $prenda3_img; ?>" />
                                        </div>
                                        <div class="w-100">
                                            <div class="sub-blue"></div>
                                        </div>
                                        <span class="text-prendas text-prendas-industrial font-text"><?php echo $prenda3; ?></span>

                                        <div class="cta">
                                            <span class="mas"><img src="_private/img/icons/mas.svg" alt="Mas" /></span>
                                            <span class="ver">
                                                Ver prendas
                                                <img src="_private/img/icons/flecha.png" alt="Ver" />
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="col-lg-3">
                                <a href="<?php echo $prenda4_url; ?>" class="tm-home-administrativas">
                                    <div class="card-prendas">
                                        <div class="border-img-prenda">
                                            <img class="img-prendas" src="<?php echo $prenda4_img; ?>" />
                                        </div>
                                        <div class="w-100">
                                            <div class="sub-blue"></div>
                                        </div>
                                        <span class="text-prendas text-prendas-oficinas font-text"><?php echo $prenda4; ?></span>

                                        <div class="cta">
                                            <span class="mas"><img src="_private/img/icons/mas.svg" alt="Mas" /></span>

                                            <span class="ver">
                                                Ver prendas
                                                <img src="_private/img/icons/flecha.png" alt="Ver" />
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>


                    <div class="disable-desk">
                        <div class="carouselMB">
                            <div class="carousel-cell">
                                <a href="<?php echo $prenda1_url; ?>" class="tm-home-hospitalaria">
                                    <div class="card-prendas">
                                        <div class="border-img-prenda">
                                            <img class="img-prendas" src="<?php echo $prenda1_img; ?>" />
                                        </div>
                                        <div class="w-100">
                                            <div class="sub-blue"></div>
                                        </div>
                                        <span class="text-prendas text-prendas-hospitales">
                                            <?php echo $prenda1; ?>
                                        </span>

                                        <div class="ctaMB">
                                            <span>
                                                Ver prendas
                                                <img src="_private/img/icons/flecha.png" alt="Ver" />
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="carousel-cell">
                                <a href="<?php echo $prenda2_url; ?>" class="tm-home-hospitalaria">
                                    <div class="card-prendas">
                                        <div class="border-img-prenda">
                                            <img class="img-prendas" src="<?php echo $prenda2_img; ?>" />
                                        </div>
                                        <div class="w-100">
                                            <div class="sub-blue"></div>
                                        </div>
                                        <span class="text-prendas text-prendas-hotel font-text"><?php echo $prenda2; ?></span>

                                        <div class="ctaMB">
                                            <span>
                                                Ver prendas
                                                <img src="_private/img/icons/flecha.png" alt="Ver" />
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="carousel-cell">
                                <a href="<?php echo $prenda3_url; ?>" class="tm-home-industriales">
                                    <div class="card-prendas">
                                        <div class="border-img-prenda">
                                            <img class="img-prendas" src="<?php echo $prenda3_img; ?>" />
                                        </div>
                                        <div class="w-100">
                                            <div class="sub-blue"></div>
                                        </div>
                                        <span class="text-prendas text-prendas-industrial font-text"><?php echo $prenda3; ?></span>

                                        <div class="ctaMB">
                                            <span>
                                                Ver prendas
                                                <img src="_private/img/icons/flecha.png" alt="Ver" />
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <div class="carousel-cell">
                                <a href="<?php echo $prenda4_url; ?>" class="tm-home-administrativas">
                                    <div class="card-prendas">
                                        <div class="border-img-prenda">
                                            <img class="img-prendas" src="<?php echo $prenda4_img; ?>" />
                                        </div>
                                        <div class="w-100">
                                            <div class="sub-blue"></div>
                                        </div>
                                        <span class="text-prendas text-prendas-oficinas font-text"><?php echo $prenda4; ?></span>

                                        <div class="ctaMB">
                                            <span>
                                                Ver prendas
                                                <img src="_private/img/icons/flecha.png" alt="Ver" />
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>                        
                    </div>

                </contenedor>
            </section>

            <img class="blue-stripe" src="_private/img/vectors/stripes.svg" alt="bg" />

            <section id="nosotros" class="porque">
                <contenedor layout-max layout-space class="inner">
                    <div class="page-title">
                        <h2 class="page-title-font">¿Por qué elegirnos?</h2>
                        <div class="w-100">
                            <div class="sub-blue"></div>
                        </div>
                    </div>

                    <div class="row fondo-main">
                        <div class="col-12 col-sm-12 col-lg-4 col-md-4">
                            <div class="conocimiento-textil disable-aos-mobile" data-aos="zoom-out-right" data-aos-offset="500">
                                <button class="button-conocimiento font-text">
                                    <img class="img-conocimientos" src="_private/img/nosotros/conocimiento.svg" />
                                    <span class="text-conocimiento">Conocimiento textil</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-lg-3 col-md-3">
                            <div class="costuteros-calificados disable-aos-mobile" data-aos="zoom-out-down">
                                <button class="button-costureros font-text">
                                    <img class="img-costureros" src="_private/img/nosotros/maquina.svg" />
                                    <span class="text-costureros">Costureros calificados</span>
                                </button>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-lg-4 col-md-3">
                            <div class="experiencia-lavado disable-aos-mobile" data-aos="zoom-out-left" data-aos-offset="500">
                                <button class="button-lavado font-text">
                                    <img class="img-lavado" src="_private/img/nosotros/lavanderia.svg" />
                                    <span class="text-lavado">Experiencia de lavado</span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="row container-msg">
                        <div class="col-xs-12 button-prendas">
                            <span class="text-prendas-costuras">Prendas y costuras que soportan procesos industriales de lavado</span>
                        </div>
                    </div>
                </contenedor>
            </section>

            <section id="tendencias" class="lanzamientos">
                <contenedor layout-max layout-space class="inner">
                    <div class="page-title">
                        <h2 class="page-title-font">Tendencias</h2>
                        <div class="w-100">
                            <div class="sub-blue"></div>
                        </div>
                    </div>

                    <div class="gallery" >
                        <div class="gallery-container" >
                            <div class="gallery-item">
                                <img src="_private/img/tendencias/houskeeping.jpg" alt="houskeeping">
                                <input type="hidden" class="product-name" value="Housekeeping" />
                                <input
                                    type="hidden"
                                    class="product-description"
                                    value="Prendas para hotelería y housekeeping. Incluye entrega en Lima y provincias. Colores, tallas y composiciones de acuerdo a la solicitud de nuestros clientes."
                                />
                            </div>
                            <div class="gallery-item">
                                <img src="_private/img/tendencias/industria.jpg" alt="industrial">
                                <input type="hidden" class="product-name" value="Prenda industrial" />
                                <input
                                    type="hidden"
                                    class="product-description"
                                    value="Prendas para la industria pesada y consumo masivo. Incluye entrega en Lima y provincias. Colores, tallas y composiciones de acuerdo a la solicitud de nuestros clientes."
                                />
                            </div>
                            <div class="gallery-item">
                                <img src="_private/img/tendencias/mascarillas.jpg" alt="mascarilla">
                                <input type="hidden" class="product-name" value="Mascarilla de Tela" />
                                <input
                                    type="hidden"
                                    class="product-description"
                                    value="Elaboramos mascarillas de tela según especificaciones MINSA y de acuerdo a la solicitud de nuestro cliente. Todas nuestras mascarillas son lavables y reutilizables. Protegen, buena duración y tienen buenos acabados."
                                />
                            </div>
                            <div class="gallery-item">
                                <img src="_private/img/tendencias/medicos.jpg" alt="medicos">
                                <input type="hidden" class="product-name" value="Médicos y enfermeras" />
                                <input
                                    type="hidden"
                                    class="product-description"
                                    value="Scrubbs, chaqueta y pantalón, mandiles de médicos y ropa asistencial. Incluye entrega en Lima y provincias. Colores, tallas y composiciones de acuerdo a solicitud de nuestros clientes."
                                />
                            </div>
                            <div class="gallery-item">
                                <img src="_private/img/tendencias/mineria.jpg" alt="mineria">
                                <input type="hidden" class="product-name" value="Minería" />
                                <input
                                    type="hidden"
                                    class="product-description"
                                    value="Prendas para la minería. Incluye entrega en Lima y/o provincias. Colores, tallas y composiciones textiles de acuerdo a solicitud de nuestros clientes."
                                />
                            </div>
                        </div>
                        <div class="gallery-controls"></div>
                    </div>
                </contenedor>
            </section>

            <section id="experiencia" class="marcas">
                <contenedor layout-max layout-space class="inner">
                    <div class="page-title">
                        <h2 class="page-title-font">Experiencia y confianza</h2>
                        <div class="w-100">
                            <div class="sub-blue"></div>
                        </div>
                    </div>

                    <div class="row">
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-sodexo" src="_private/img/clientes/sodexo.svg" alt="Sodexo" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-ceelimp" src="_private/img/clientes/ceelimp.svg" alt="Ceelimp" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-sanna" src="_private/img/clientes/sanna.svg" alt="Sanna" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-sanfelipe" src="_private/img/clientes/sanfelipe.svg" alt="San Felipe" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-ricardo-palma" src="_private/img/clientes/ricardopalma.svg" alt="Ricardo Palma" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-oncosalud" src="_private/img/clientes/oncosalud.svg" alt="Oncosalud" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-nestle" src="_private/img/clientes/nestle.svg" alt="Nestle" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-stella-maris" src="_private/img/clientes/maris.svg" alt="Maris" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-lezza" src="_private/img/clientes/lezza.svg" alt="Lezza" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-internacional" src="_private/img/clientes/internacional.svg" alt="Internacional" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-centenario" src="_private/img/clientes/centenario.svg" alt="Centenario" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-auna" src="_private/img/clientes/auna.svg" alt="Auna" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-auna" src="_private/img/clientes/carita-feliz.svg" alt="Auna" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-auna" src="_private/img/clientes/ibermansa.svg" alt="Auna" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-auna" src="_private/img/clientes/insm.svg" alt="Auna" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-auna" src="_private/img/clientes/ipsos.svg" alt="Auna" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-auna" src="_private/img/clientes/santa-isabel.svg" alt="Auna" />
                        <img class="col-6 col-sm-4 col-md-3 col-xl-2 img-auna" src="_private/img/clientes/viva.svg" alt="Auna" />
                    </div>
                </contenedor>
            </section>

            <section id="blog" class="marcas">
                <contenedor layout-max layout-space class="inner">
                    <div class="page-title">
                        <h2 class="page-title-font">Entérate sobre el mundo textil</h2>
                        <div class="w-100">
                            <div class="sub-blue"></div>
                        </div>
                    </div>
                 
                    <div class="container owl-carousel-news px-4 pt-3" data-aos="zoom-in" data-aos-offset="300" id="news">
                        <!-- blogs -->
                    </div>
                </contenedor>
            </section>
        </main>

        <!-- Footer -->
        <?php include '_partials/footer.php';?>
        <!-- /Footer -->

       
        <div class="modal" id="modal">
            <div class="modal-dialog font-text" role="document">
                <div class="modal-content p-4">
                    <button type="button" class="close modal-close" data-dismiss="modal" aria-label="Close">&times;</button>
                    <div class="modal-body d-flex justify-content-center flex-column align-items-center">
                        
                        <div class="modal-tendencias">
                            <img src="" class="modal-tendencias-img">
                            <div class="modal-tendencias-content">
                                <div class="modal-tendencias-title"></div>
                                <div class="modal-tendencias-desc"></div>
                                <div class="modal-tendencias-button">
                                    
                                    <a target="_blank" href="https://wa.me/+51962927662" data-animation="animated zoomIn" class="button button-first tm-tendencias-cotizar">
                                        <img src="_private/img/socialmedia/whatsapp.svg" alt="icon" class="button-icon" />
                                        Cotizar ahora
                                    </a>

                                    <div class="modal-tendencias-or">ó</div>

                                    <a href="tel:+51962927662" class="modal-tendencias-call"><img src="_private/img/socialmedia/llamar.png" alt="icon" class="modal-tendencias-call--is-icon" /> 962927662</a>                                    
                                    
                                </div>
                            </div>           
                        </div>
                    </div>                    
                </div>
            </div>
        </div>

        <!-- Scripts -->
        <?php include '_partials/scripts.php';?>
        <!-- /Scripts -->

        <script src="<?php echo $host ?>_private/vendors/gsap/cssplugins.min.js"></script>
        <script src="<?php echo $host ?>_private/vendors/gsap/easepack.min.js"></script>
        <script src="<?php echo $host ?>_private/vendors/gsap/tweenlite.min.js"></script>
        <script src="<?php echo $host ?>_private/vendors/gsap/timelinelite.min.js"></script>
        <script src="<?php echo $host ?>_private/vendors/animate/animatejs.min.js"></script>
        <script src="<?php echo $host ?>_private/vendors/flickity/flickity.pkgd.min.js"></script>
       
    </body>
</html>
