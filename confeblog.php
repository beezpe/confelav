<?php 
    include './_webconfig/root.php';
    include './_webconfig/prendas.php';
    $page_title = "Confelav  Confecciones de Lavanderia";
?>

<!DOCTYPE html>

<html lang="es" class="confeblog">

<head>
    <?php include './_partials/head.php' ?>
</head>

<body>
    <!-- Header -->
    <?php include './_partials/header.php';?>
    <!-- /Header -->

    <main class="main-content richtext">
        <img class="blue-stripe" src="_private/img/vectors/fondo.svg" alt="Bg">

        <contenedor layout-max layout-space>
            <section class="section-tabs">
                
                <div class="row contenT">
                    <div class="col-md-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">

                                    <div style="text-align: center; margin: 0 auto;">
                                        <h1 class="font-mundo-textil">
                                            Entérate sobre el mundo textil
                                        </h1>
                                        <div class="w-100">
                                            <div class="sub-blueT"></div>
                                        </div>
                                    </div> 
                                </div>
                            </div>

                            <!-- <div class="col-md-4">
                                <div class="form-group has-search">
                                    <img src="_private/img/icons/lupa.svg" alt="icon" class="form-control-feedback">
                                    <input type="text" placeholder="¿Qué estás buscando?">
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>

                <div class="contenT">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tabs">
                                <ul class="filters-button-group">
                                    <li class="grid_sorting_button active is-checked" data-filter="*">
                                        <div type="button" class="button tags">
                                            <img src="_private/img/icons/tag1.svg" alt="icon"> 
                                            Todos
                                        </div>
                                    </li>
                                    <li class="grid_sorting_button is-checked" data-filter=".prendas">
                                        <div type="button" class="button tags">
                                            <img src="_private/img/icons/tag1.svg" alt="icon"> 
                                            Hospitalarias
                                        </div>
                                    </li>
                                    <li class="grid_sorting_button is-checked" data-filter=".mascarillas" id="hotelera">
                                        <div type="button" class="button tags">
                                            <img src="_private/img/icons/tag1.svg" alt="icon"> 
                                            Hoteleras
                                        </div>
                                    </li>
                                    <li class="grid_sorting_button is-checked" data-filter=".lavanderia">
                                        <div type="button" class="button tags">
                                            <img src="_private/img/icons/tag1.svg" alt="icon"> 
                                            Industriales
                                        </div>
                                    </li>
                                    <li class="grid_sorting_button is-checked" data-filter=".industrial">
                                        <div type="button" class="button tags">
                                            <img src="_private/img/icons/tag1.svg" alt="icon"> 
                                            Administrativas
                                        </div>
                                    </li>
                                    <li class="grid_sorting_button is-checked" data-filter=".variados">
                                        <div type="button" class="button tags">
                                            <img src="_private/img/icons/tag1.svg" alt="icon"> 
                                            Variados
                                        </div>
                                    </li>
                                </ul>

                                <div class="row">
                                    <div class="col contents">
                                        <div id="product-grid" class="product-grid" data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'>

                                            <!-- Blogs-->
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </contenedor>
    </main>

    <!-- Footer -->
    <?php include '_partials/footer.php';?>
    <!-- /Footer -->

    <!-- Scripts -->
    <?php include '_partials/scripts.php';?>
    <!-- /Scripts -->
   
   

</body>


</html>