<?php 
    include '../_webconfig/root.php';
    include '../_webconfig/prendas.php';
    $page_title = "Confelav  Confecciones de Lavanderia";
?>

<!DOCTYPE html>

<html lang="es">

<head>
    <?php include '../_partials/head.php' ?>
</head>

<body>
    <!-- Header -->
    <?php include '../_partials/header.php';?>
    <!-- /Header -->


    <main class="main-content">
        <img class="blue-stripe" src="_private/img/vectors/fondo.svg" alt="Bg">

        <contenedor layout-max>
            <section class="section-tabs">
            
                <div class="row">
                    <div class="col-md-1 col-sm-2">
                        <img class="img-title" src="<?php echo $prenda3_img; ?>" alt="<?php echo $prenda3; ?>">
                    </div>

                    <div class="col-md-11 col-sm-10">
                        <h1 class="font-mundo-textil">
                            <?php echo $prenda3; ?>
                        </h1>
                        <div class="w-100">
                            <div class="sub-blueT"></div>
                        </div>

                    </div>
                </div>
              
                <div class="tab-content">
                    <div class="tab-pane active" id="industriales">

                        <img class="sala" src="<?php echo $prenda3_cat1_img; ?>" style="width:75%">
               
                        <div class="sala-content">

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/industriales/polo.svg" alt="Polo">
                                    <span class="sala-items-header-title">Polo<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Industria</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/industriales/polera.svg" alt="Polera ">
                                    <span class="sala-items-header-title">Polera<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Industria</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                    

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/industriales/pantalon.svg" alt="Pantalón">
                                    <span class="sala-items-header-title">Pantalón<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Industria</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/industriales/casaca.svg" alt="Casaca">
                                    <span class="sala-items-header-title">Casaca<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Industria</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/industriales/chompa.svg" alt="Chompa">
                                    <span class="sala-items-header-title">Chompa<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Industria</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/industriales/overol.svg" alt="Overol">
                                    <span class="sala-items-header-title">Overol<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Industria</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>


                            


                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/industriales/chaleco.svg" alt="Chaleco">
                                    <span class="sala-items-header-title">Chaleco<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Industria</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>


                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/industriales/cofia.svg" alt="Cofia">
                                    <span class="sala-items-header-title">Cofia<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Industria</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>


                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/industriales/medias.svg" alt="Medias">
                                    <span class="sala-items-header-title">Medias<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Industria</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/industriales/botas.svg" alt="Botas de Seguridad">
                                    <span class="sala-items-header-title">Botas de Seguridad<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Industria</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/icons/otros.svg" alt="Otros">
                                    <span class="sala-items-header-title">Otros<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Otras prendas, colores y composiciones a solicitud de nuestros clientes</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>


                        <div class="prendas-cotizar" data-animation="animated zoomIn">

                            <h3 class="prendas-nota"> <strong>Nota:</strong> Medidas, colores y tipos de telas referenciales y/o a solicitud del cliente. Incluye bordado o estampado del logo institucional.</h3>
            
                            <a target="_blank" href="https://wa.me/+51962927662" data-animation="animated zoomIn" class="button button-first tm-industriales-cotizar">
                                <img src="_private/img/icons/telefono.svg" alt="icon" class="button-icon" />
                                Cotizar ahora
                            </a>
                        </div>


                    </div>

               
                </div>


                <div class="referencias">
                    <div class="referencias-tabs row">
                        <a href="<?php echo $prenda1_url; ?>" class="referencias-tabs-button col-md-3 col-sm-12">
                            <img src="<?php echo $prenda1_img; ?>" alt="<?php echo $prenda1; ?>" class="referencias-tabs-img">
                            <?php echo $prenda1; ?>
                        </a>
                        <a href="<?php echo $prenda2_url; ?>" class="referencias-tabs-button col-md-3 col-sm-12">
                            <img src="<?php echo $prenda2_img; ?>" alt="<?php echo $prenda2; ?>" class="referencias-tabs-img">
                            <?php echo $prenda2; ?>
                        </a>
                        <a href="<?php echo $prenda4_url; ?>" class="referencias-tabs-button col-md-3 col-sm-12">
                            <img src="<?php echo $prenda4_img; ?>" alt="<?php echo $prenda4; ?>" class="referencias-tabs-img">
                            <?php echo $prenda4; ?>
                        </a>
                    </div>
                </div>

            </section>
        </contenedor>
        
    </main>

    <!-- Footer -->
   <?php include '../_partials/footer.php';?>
    <!-- /Footer -->

    
    <!-- Scripts -->
    <?php include '../_partials/scripts.php';?>
    <!-- /Scripts -->
    
</body>



</html>