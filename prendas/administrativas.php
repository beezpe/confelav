<?php 
    include '../_webconfig/root.php';
    include '../_webconfig/prendas.php';
    $page_title = "Confelav  Confecciones de Lavanderia";
?>

<!DOCTYPE html>

<html lang="es">

<head>
    <?php include '../_partials/head.php' ?>
</head>

<body>
    <!-- Header -->
    <?php include '../_partials/header.php';?>
    <!-- /Header -->


    <main class="main-content">
        <img class="blue-stripe" src="_private/img/vectors/fondo.svg" alt="Bg">

        <contenedor layout-max>
            <section class="section-tabs">
            
                <div class="row">
                    <div class="col-md-1 col-sm-2">
                        <img class="img-title" src="<?php echo $prenda4_img; ?>" alt="<?php echo $prenda4; ?>">
                    </div>

                    <div class="col-md-11 col-sm-10">
                        <h1 class="font-mundo-textil">
                            <?php echo $prenda4; ?>
                        </h1>
                        <div class="w-100">
                            <div class="sub-blueT"></div>
                        </div>

                    </div>
                </div>

                   
                <div class="tab-content">
                    <div class="tab-pane active" id="administrativas">

                        <img class="sala" src="<?php echo $prenda4_cat1_img; ?>">
               
                        <div class="sala-content">

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/administrativas/camisa.svg" alt="Camisa">
                                    <span class="sala-items-header-title">Camisa<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Oficina</span></li>                                   
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/administrativas/pantalon.svg" alt="Pantalón">
                                    <span class="sala-items-header-title">Pantalón<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Oficina</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/administrativas/falda.svg" alt="Falda">
                                    <span class="sala-items-header-title">Falda<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Oficina</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/administrativas/chaleco.svg" alt="Chaleco">
                                    <span class="sala-items-header-title">Chaleco<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Oficina</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/administrativas/chaqueta.svg" alt="Chaqueta de dama">
                                    <span class="sala-items-header-title">Chaqueta de dama<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Oficina</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/administrativas/saco-caballero.svg" alt="Saco de caballero">
                                    <span class="sala-items-header-title">Saco de caballero<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Oficina</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/icons/otros.svg" alt="Otros">
                                    <span class="sala-items-header-title">Otros<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Otras prendas, colores y composiciones a solicitud de nuestros clientes</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            

                        </div>

                        <div class="prendas-cotizar" data-animation="animated zoomIn">

                            <h3 class="prendas-nota"> <strong>Nota:</strong> Medidas, colores y tipos de telas referenciales y/o a solicitud del cliente. Incluye bordado o estampado del logo institucional.</h3>
            
                            <a target="_blank" href="https://wa.me/+51962927662" data-animation="animated zoomIn" class="button button-first tm-administrativas-cotizar">
                                <img src="_private/img/icons/telefono.svg" alt="icon" class="button-icon" />
                                Cotizar ahora
                            </a>
                        </div>


                    </div>

                    

                </div>






                <div class="referencias">
                    <div class="referencias-tabs row">
                        <a href="<?php echo $prenda1_url; ?>" class="referencias-tabs-button col-md-3 col-sm-12">
                            <img src="<?php echo $prenda1_img; ?>" alt="<?php echo $prenda1; ?>" class="referencias-tabs-img">
                            <?php echo $prenda1; ?>
                        </a>
                        <a href="<?php echo $prenda2_url; ?>" class="referencias-tabs-button col-md-3 col-sm-12">
                            <img src="<?php echo $prenda2_img; ?>" alt="<?php echo $prenda2; ?>" class="referencias-tabs-img">
                            <?php echo $prenda2; ?>
                        </a>
                        <a href="<?php echo $prenda3_url; ?>" class="referencias-tabs-button col-md-3 col-sm-12">
                            <img src="<?php echo $prenda3_img; ?>" alt="<?php echo $prenda3; ?>" class="referencias-tabs-img">
                            <?php echo $prenda3; ?>
                        </a>
                    </div>
                </div>

            </section>
        </contenedor>
        
    </main>

    <!-- Footer -->
   <?php include '../_partials/footer.php';?>
    <!-- /Footer -->

    
    <!-- Scripts -->
    <?php include '../_partials/scripts.php';?>
    <!-- /Scripts -->
    
</body>



</html>