<?php 
    include '../_webconfig/root.php';
    include '../_webconfig/prendas.php';
    $page_title = "Confelav  Confecciones de Lavanderia";
?>

<!DOCTYPE html>

<html lang="es">

<head>
    <?php include '../_partials/head.php' ?>
</head>

<body>
    <!-- Header -->
    <?php include '../_partials/header.php';?>
    <!-- /Header -->


    <main class="main-content">
        <img class="blue-stripe" src="_private/img/vectors/fondo.svg" alt="Bg">

        <contenedor layout-max>
            <section class="section-tabs">
            
                <div class="row">
                    <div class="col-md-1 col-sm-2">
                        <img class="img-title" src="<?php echo $prenda1_img; ?>" alt="<?php echo $prenda1; ?>">
                    </div>

                    <div class="col-md-11 col-sm-10">
                        <h1 class="font-mundo-textil">
                            <?php echo $prenda1; ?>
                        </h1>
                        <div class="w-100">
                            <div class="sub-blueT"></div>
                        </div>

                        <ul class="my-tabs">
                            <li class="active">
                                <a href="#hospitalaria" data-toggle="tab">
                                    <div class="my-tabs-button">
                                        <?php echo $prenda1_cat3; ?>
                                     </div>
                                </a>
                            </li>
                            
                            <li>
                                <a href="#sop" data-toggle="tab">
                                    <div class="my-tabs-button">
                                        <?php echo $prenda1_cat2; ?>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#medicos" data-toggle="tab">
                                    <div class="my-tabs-button">
                                        <?php echo $prenda1_cat1; ?>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#neo" data-toggle="neo">
                                    <div class="my-tabs-button">
                                        <?php echo $prenda1_cat4; ?>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                   
                <div class="tab-content">
                    <div class="tab-pane active" id="hospitalaria">

                        <img class="sala" src="<?php echo $prenda1_cat3_img; ?>">
               
                        <div class="sala-content">


                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/hospitalaria/sabana-simple.svg" alt="Sábana Simple">
                                    <span class="sala-items-header-title">Sábana Simple<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hospitalizacion</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/hospitalaria/sabana-amoldable.svg" alt="Sábana Amoldable">
                                    <span class="sala-items-header-title">Sábana Amoldable<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hospitalizacion</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                             <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/hospitalaria/soleras.svg" alt="Solera">
                                    <span class="sala-items-header-title">Solera<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hospitalizacion</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/hospitalaria/almohada.svg" alt="Almohada">
                                    <span class="sala-items-header-title">Almohada<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hospitalización</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/hospitalaria/funda-de-almohada.svg" alt="Funda de Almohada">
                                    <span class="sala-items-header-title">Funda de Almohada<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hospitalizacion</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/hospitalaria/camisa-de-paciente.svg" alt="Camisa de paciente ">
                                    <span class="sala-items-header-title">Camisa de paciente<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hospitalización</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/hospitalaria/cubrecama.svg" alt="Cubrecama">
                                    <span class="sala-items-header-title">Cubrecama<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hospitalizacion</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/hospitalaria/frazada.svg" alt="Almohada">
                                    <span class="sala-items-header-title">Frazada<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hospitalizacion</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/hospitalaria/sabana-simple.svg" alt="Secador">
                                    <span class="sala-items-header-title">Secador<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hospitalizacion</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/hospitalaria/toalla-de-cuerpos.svg" alt="Toalla de Cuerpos">
                                    <span class="sala-items-header-title">Toalla de Cuerpo<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Baños</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/hospitalaria/toalla-de-manos.svg" alt="Toalla de Manos">
                                    <span class="sala-items-header-title">Toalla de Mano<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Baños</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/hospitalaria/toalla-de-piso.svg" alt="Toalla de Piso">
                                    <span class="sala-items-header-title">Toalla de Piso<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Baños</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/icons/otros.svg" alt="Otros">
                                    <span class="sala-items-header-title">Otros<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Otras prendas, colores y composiciones a solicitud de nuestros clientes</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="prendas-cotizar" data-animation="animated zoomIn">

                            <h3 class="prendas-nota"> <strong>Nota:</strong> Medidas, colores y tipos de telas referenciales y/o a solicitud del cliente. Incluye bordado o estampado del logo institucional.</h3>
            
                            <a target="_blank" href="https://wa.me/+51962927662" data-animation="animated zoomIn" class="button button-first tm-hospitalaria-cotizar">
                                <img src="_private/img/icons/telefono.svg" alt="icon" class="button-icon" />
                                Cotizar ahora
                            </a>
                        </div>


                    </div>

                    <div class="tab-pane" id="sop">
                        <img class="sala" src="<?php echo $prenda1_cat2_img; ?>">

                        <div class="sala-content">
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/sop/sabana-simple-sop.svg" alt="Sábana Simple">
                                    <span class="sala-items-header-title">Sábana Simple<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/sop/sabana-amoldable-sop.svg" alt="Sábana Amoldable">
                                    <span class="sala-items-header-title">Sábana Amoldable<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/sop/campo-simple.svg" alt="Campo Simple">
                                    <span class="sala-items-header-title">Campo Simple<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/sop/funda-de-mayo.svg" alt="Funda de Mayo">
                                    <span class="sala-items-header-title">Funda de Mayo<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/sop/mandil.svg" alt="Mandil">
                                    <span class="sala-items-header-title">Mandil<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/sop/bolsa-laparoscopica.svg" alt="Bolsa Laparoscópica">
                                    <span class="sala-items-header-title">Bolsa Laparoscópica<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/sop/envoltorio.svg" alt="Envoltorio">
                                    <span class="sala-items-header-title">Envoltorio<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/sop/poncho.svg" alt="Poncho">
                                    <span class="sala-items-header-title">Poncho<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/sop/campo-de-medicina.svg" alt="Campo de Medicina">
                                    <span class="sala-items-header-title">Campo de Medicina<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/sop/campo-fenestrado.svg" alt="Campo Fenestrado">
                                    <span class="sala-items-header-title">Campo Fenestrado<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/icons/otros.svg" alt="Otros">
                                    <span class="sala-items-header-title">Otros<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Otras prendas, colores y composiciones a solicitud de nuestros clientes</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        </div>

                        <div class="prendas-cotizar" data-animation="animated zoomIn">

                            <h3 class="prendas-nota"> <strong>Nota:</strong> Medidas, colores y tipos de telas referenciales y/o a solicitud del cliente. Incluye bordado o estampado del logo institucional.</h3>
            
                            <a target="_blank" href="https://wa.me/+51962927662" data-animation="animated zoomIn" class="button button-first tm-sop-cotizar">
                                <img src="_private/img/icons/telefono.svg" alt="icon" class="button-icon" />
                                Cotizar ahora
                            </a>
                        </div>
                    </div>


                    <div class="tab-pane" id="medicos">
                        <img class="sala" src="<?php echo $prenda1_cat1_img; ?>">

                        <div class="sala-content">

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/sop/mandil.svg" alt="Mandil">
                                    <span class="sala-items-header-title">Mandil<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/medico-enfermera/bata-de-medico.svg" alt="Bata de Médico">
                                    <span class="sala-items-header-title">Bata de Médico<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Uso general</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/medico-enfermera/chaqueta-de-cirujano.svg" alt="Chaqueta de Cirujano">
                                    <span class="sala-items-header-title">Chaqueta de Cirujano<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/medico-enfermera/pantalon-de-cirujano.svg" alt="Pantalón de Cirujano">
                                    <span class="sala-items-header-title">Pantalón de Cirujano<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operacionesn</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/medico-enfermera/chaqueta-de-enfermero.svg" alt="Chaqueta de Enfermería">
                                    <span class="sala-items-header-title">Chaqueta de Enfermería<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operaciones</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/medico-enfermera/pantalon-de-enfermero.svg" alt="Pantalón de Enfermería">
                                    <span class="sala-items-header-title">Pantalón de Enfermería<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Sala de Operacionesn</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/medico-enfermera/mascarilla.svg" alt="Mascarilla">
                                    <span class="sala-items-header-title">Mascarilla<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Uso general</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/medico-enfermera/cofia.svg" alt="Cofia">
                                    <span class="sala-items-header-title">Cofia<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Uso general</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/icons/otros.svg" alt="Otros">
                                    <span class="sala-items-header-title">Otros<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Otras prendas, colores y composiciones a solicitud de nuestros clientes</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="prendas-cotizar" data-animation="animated zoomIn">

                            <h3 class="prendas-nota"> <strong>Nota:</strong> Medidas, colores y tipos de telas referenciales y/o a solicitud del cliente. Incluye bordado o estampado del logo institucional.</h3>
            
                            <a target="_blank" href="https://wa.me/+51962927662" data-animation="animated zoomIn" class="button button-first tm-medicos-cotizar">
                                <img src="_private/img/icons/telefono.svg" alt="icon" class="button-icon" />
                                Cotizar ahora
                            </a>
                        </div>

                    </div>


                    <div class="tab-pane" id="neo">
                        <img class="sala" src="<?php echo $prenda1_cat4_img; ?>">

                        <div class="sala-content">

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/neo/enterizo.svg" alt="Enterizo">
                                    <span class="sala-items-header-title">Enterizo<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Neonatal</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/neo/body.svg" alt="Body">
                                    <span class="sala-items-header-title">Body<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Neonatal</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/neo/babita.svg" alt="Babita">
                                    <span class="sala-items-header-title">Babita<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Neonatal</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/neo/babero.svg" alt="Babero">
                                    <span class="sala-items-header-title">Babero<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Neonatal</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/neo/gorrito.svg" alt="Gorrito">
                                    <span class="sala-items-header-title">Gorrito<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Neonatal</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/neo/manoplas.svg" alt="Manoplas">
                                    <span class="sala-items-header-title">Manoplas<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Neonatal</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/neo/polo.svg" alt="Polo">
                                    <span class="sala-items-header-title">Polo<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Neonatal</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/neo/pantalon.svg" alt="Pantalón">
                                    <span class="sala-items-header-title">Pantalón<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Neonatal</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/neo/toalla.svg" alt="Toalla">
                                    <span class="sala-items-header-title">Toalla<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Neonatal</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/neo/manta.svg" alt="Manta">
                                    <span class="sala-items-header-title">Mantita<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Neonatal</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hospitalarias/neo/medias.svg" alt="Medias">
                                    <span class="sala-items-header-title">Medias<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Neonatal</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/icons/otros.svg" alt="Otros">
                                    <span class="sala-items-header-title">Otros<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Otras prendas, colores y composiciones a solicitud de nuestros clientes</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>                          
                        </div>

                        <div class="prendas-cotizar" data-animation="animated zoomIn">

                            <h3 class="prendas-nota"> <strong>Nota:</strong> Medidas, colores y tipos de telas referenciales y/o a solicitud del cliente. Incluye bordado o estampado del logo institucional.</h3>
            
                            <a target="_blank" href="https://wa.me/+51962927662" data-animation="animated zoomIn" class="button button-first tm-neo-cotizar">
                                <img src="_private/img/icons/telefono.svg" alt="icon" class="button-icon" />
                                Cotizar ahora
                            </a>
                        </div>
                    </div>
                            

                    
                </div>


                <div class="referencias">
                    <div class="referencias-tabs row">
                        <a href="<?php echo $prenda2_url; ?>" class="referencias-tabs-button col-md-3 col-sm-12">
                            <img src="<?php echo $prenda2_img; ?>" alt="<?php echo $prenda2; ?>" class="referencias-tabs-img">
                            <?php echo $prenda2; ?>
                        </a>
                        <a href="<?php echo $prenda3_url; ?>" class="referencias-tabs-button col-md-3 col-sm-12">
                            <img src="<?php echo $prenda3_img; ?>" alt="<?php echo $prenda3; ?>" class="referencias-tabs-img">
                            <?php echo $prenda3; ?>
                        </a>
                        <a href="<?php echo $prenda4_url; ?>" class="referencias-tabs-button col-md-3 col-sm-12">
                            <img src="<?php echo $prenda4_img; ?>" alt="<?php echo $prenda4; ?>" class="referencias-tabs-img">
                            <?php echo $prenda4; ?>
                        </a>
                    </div>
                </div>

            </section>
        </contenedor>
        
    </main>

    <!-- Footer -->
   <?php include '../_partials/footer.php';?>
    <!-- /Footer -->

    
    <!-- Scripts -->
    <?php include '../_partials/scripts.php';?>
    <!-- /Scripts -->
    
</body>



</html>