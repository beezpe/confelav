<?php 
    include '../_webconfig/root.php';
    include '../_webconfig/prendas.php';
    $page_title = "Confelav  Confecciones de Lavanderia";
?>

<!DOCTYPE html>

<html lang="es">

<head>
    <?php include '../_partials/head.php' ?>
</head>

<body>
    <!-- Header -->
    <?php include '../_partials/header.php';?>
    <!-- /Header -->


    <main class="main-content">
        <img class="blue-stripe" src="_private/img/vectors/fondo.svg" alt="Bg">

        <contenedor layout-max>
            <section class="section-tabs">
            
                <div class="row">
                    <div class="col-md-1 col-sm-2">
                        <img class="img-title" src="<?php echo $prenda2_img; ?>" alt="<?php echo $prenda2; ?>">
                    </div>

                    <div class="col-md-11 col-sm-10">
                        <h1 class="font-mundo-textil">
                            <?php echo $prenda2; ?>
                        </h1>
                        <div class="w-100">
                            <div class="sub-blueT"></div>
                        </div>

                        <ul class="my-tabs">
                            <li class="active">
                                <a href="#hoteleria" data-toggle="tab">
                                    <div class="my-tabs-button">
                                        <?php echo $prenda2_cat1; ?>
                                     </div>
                                </a>
                            </li>
                            
                            <li>
                                <a href="#housekeeping" data-toggle="tab">
                                    <div class="my-tabs-button">
                                        <?php echo $prenda2_cat2; ?>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>

                   
                <div class="tab-content">
                    <div class="tab-pane active" id="hoteleria">

                        <img class="sala" src="<?php echo $prenda2_cat1_img; ?>">
               
                        <div class="sala-content">                            
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/sabana-amoldable.svg" alt="Juego de Sábanas">
                                    <span class="sala-items-header-title">Juego de Sábanas<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hotel</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/protector-colchon.svg" alt="Protector de Colchón">
                                    <span class="sala-items-header-title">Protector de Colchón<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hotel</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/almohada.svg" alt="Almohada">
                                    <span class="sala-items-header-title">Almohada<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de hotel</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/funda-de-almohada.svg" alt="Funda de Almohada">
                                    <span class="sala-items-header-title">Funda de Almohada<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hotel</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/cojin.svg" alt="Cojín">
                                    <span class="sala-items-header-title">Cojín<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de hotel</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/edredon.svg" alt="Edredón">
                                    <span class="sala-items-header-title">Edredón<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hotel</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/colcha.svg" alt="Colcha ">
                                    <span class="sala-items-header-title">Colcha<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hotel</span></li>
                                    <li>Color: <i class="white"></i> <i class="skyblue"></i> <i class="green"></i></li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/frazada.svg" alt="frazada">
                                    <span class="sala-items-header-title">Duvet<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Cama de Hotel</span></li>
                                    <li>Color: <i class="skyblue"></i> <i class="green"></i><i class="brown"></i><i class="gray"></i></li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/toalla-de-cuerpo.svg" alt="Toalla de Cuerpos">
                                    <span class="sala-items-header-title">Toalla de Cuerpo<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Baño</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/toalla-de-mano.svg" alt="Toalla de Manos">
                                    <span class="sala-items-header-title">Toalla de Mano<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Baño</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>


                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/toalla-de-piso.svg" alt="Toalla de Piso">
                                    <span class="sala-items-header-title">Toalla de Piso<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Baño</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/bata-de-bano.svg" alt="Bata de baño">
                                    <span class="sala-items-header-title">Bata de baño<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Baño</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/toalla-de-cuerpo.svg" alt="Toalla de Cuerpos">
                                    <span class="sala-items-header-title">Toalla de Piscina<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Piscina</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/hoteleria/toalla-de-gimnasio.svg" alt="Toalla de Cuerpos">
                                    <span class="sala-items-header-title">Toalla de Gimnasio<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Gimnasio</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/icons/otros.svg" alt="Otros">
                                    <span class="sala-items-header-title">Otros<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Otras prendas, colores y composiciones a solicitud de nuestros clientes</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>


                        <div class="prendas-cotizar" data-animation="animated zoomIn">

                            <h3 class="prendas-nota"> <strong>Nota:</strong> Medidas, colores y tipos de telas referenciales y/o a solicitud del cliente. Incluye bordado o estampado del logo institucional.</h3>
            
                            <a target="_blank" href="https://wa.me/+51962927662" data-animation="animated zoomIn" class="button button-first tm-hotelera-cotizar">
                                <img src="_private/img/icons/telefono.svg" alt="icon" class="button-icon" />
                                Cotizar ahora
                            </a>
                        </div>



                    </div>


                    <div class="tab-pane" id="housekeeping">

                        <img class="sala" src="<?php echo $prenda2_cat2_img; ?>">
               
                        <div class="sala-content">

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/housekeeping/gobernata.svg" alt="Otros">
                                    <span class="sala-items-header-title">Gobernanta<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Hoteles</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/housekeeping/floor-manager.svg" alt="Otros">
                                    <span class="sala-items-header-title">Floor managers<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Hoteles</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/housekeeping/supervisores.svg" alt="Otros">
                                    <span class="sala-items-header-title">Supervisores<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Hoteles</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/housekeeping/camarera.svg" alt="Otros">
                                    <span class="sala-items-header-title">Camareras de habitaciones<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Hoteles</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/housekeeping/valets.svg" alt="Otros">
                                    <span class="sala-items-header-title">Valets<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Hoteles</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/housekeeping/limpiadores.svg" alt="Otros">
                                    <span class="sala-items-header-title">Limpiadores<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Hoteles</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/housekeeping/office-cordinator.svg" alt="Otros">
                                    <span class="sala-items-header-title">Office Coordinator<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Hoteles</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/prendas/hoteleras/housekeeping/administracion.svg" alt="Otros">
                                    <span class="sala-items-header-title">Administración<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Hoteles</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="sala-content-items">
                                <div class="sala-items-header">
                                    <img class="sala-items-header-img" src="_private/img/icons/otros.svg" alt="Otros">
                                    <span class="sala-items-header-title">Otros<img src="_private/img/icons/arrow.svg" class="sala-items-heade-arrow"></span>
                                </div>                            
                                <ul class="sala-items-body" style="display: none;">
                                    <li>Uso: <span>Otras prendas, colores y composiciones a solicitud de nuestros clientes</span></li>
                                    <li>Color: 
                                        <div class="sala-items-colors">
                                            <?php include "../_partials/colors.php" ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            
                            
                        </div>


                        <div class="prendas-cotizar" data-animation="animated zoomIn">

                            <h3 class="prendas-nota"> <strong>Nota:</strong> Medidas, colores y tipos de telas referenciales y/o a solicitud del cliente. Incluye bordado o estampado del logo institucional.</h3>
            
                            <a target="_blank" href="https://wa.me/+51962927662" data-animation="animated zoomIn" class="button button-first tm-housekeeping-cotizar">
                                <img src="_private/img/icons/telefono.svg" alt="icon" class="button-icon" />
                                Cotizar ahora
                            </a>
                        </div>
                    </div>
                </div>


                <div class="referencias">
                    <div class="referencias-tabs row">
                        <a href="<?php echo $prenda1_url; ?>" class="referencias-tabs-button col-md-3 col-sm-12">
                            <img src="<?php echo $prenda1_img; ?>" alt="<?php echo $prenda1; ?>" class="referencias-tabs-img">
                            <?php echo $prenda1; ?>
                        </a>
                        <a href="<?php echo $prenda3_url; ?>" class="referencias-tabs-button col-md-3 col-sm-12">
                            <img src="<?php echo $prenda3_img; ?>" alt="<?php echo $prenda3; ?>" class="referencias-tabs-img">
                            <?php echo $prenda3; ?>
                        </a>
                        <a href="<?php echo $prenda4_url; ?>" class="referencias-tabs-button col-md-3 col-sm-12">
                            <img src="<?php echo $prenda4_img; ?>" alt="<?php echo $prenda4; ?>" class="referencias-tabs-img">
                            <?php echo $prenda4; ?>
                        </a>
                    </div>
                </div>

            </section>
        </contenedor>
        
    </main>

    <!-- Footer -->
   <?php include '../_partials/footer.php';?>
    <!-- /Footer -->

    
    <!-- Scripts -->
    <?php include '../_partials/scripts.php';?>
    <!-- /Scripts -->
    
</body>



</html>