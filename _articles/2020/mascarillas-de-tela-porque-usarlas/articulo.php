
<blog-banner style="background-image:url(_private/articulos/2020/mascarillas-de-tela-porque-usarlas//mascarilla-de-tela.jpg);"></blog-banner>

<h1>Mascarillas de tela: Por qué usarlas y cómo hacerlas.</h1>

<blog-date><img src="_private/img/icons/calendar.svg"> Fecha: 26/11/2020</blog-date>



<p>Las instituciones que se encargan del control y la prevención de enfermedades recomiendan que las personas que van a lugares públicos donde el distanciamiento físico es difícil deben cubrirse la cara con <strong> mascarillas de tela</strong>, quirúrgicas, o confeccionadas con otro material, independientemente de si están enfermas o no.</p>
<p>También se les pedirá que se cubran la cara cuando ingresen a cualquier instalación de atención médica, y algunos otros negocios, como los salones de belleza, donde también se requiere que se cubran la cara al entrar.</p>
<p>Las <strong> mascarillas de tela</strong> ayudan a frenar la propagación del virus ya que reducen el riesgo de exposición potencial de una persona infectada, tenga síntomas o no.</p>
<p>Aquellas personas que usan <strong>mascarillas de tela</strong> , cubrebocas quirúrgicos, o tapabocas sencillos, están protegidas contra la infección.</p>
<p>Ahora bien, las <strong> máscaras de tela</strong> bloquean la liberación de partículas respiratorias exhaladas al medio ambiente, junto con los microorganismos que estas partículas transportan.</p>
<h2>¿Por qué usar mascarillas de tela?</h2>
<p>Las <strong>máscaras de tela </strong>  no sólo bloquean eficazmente la mayoría de las gotas grandes (es decir, de 20 a 30 micrones y más), sino que también pueden bloquear la exhalación de finas gotas y partículas (también conocidas como aerosoles) de menos de 10 micrones; que aumentan en número con el volumen del habla y los tipos específicos de fonación.</p>
<p>Actualmente, existen <strong> máscaras de tela </strong> multicapa que pueden bloquear hasta el 50-70% de estas gotas y partículas finas, y limitar la propagación hacia adelante de las que no se capturan.</p>
<p>Se ha logrado un bloqueo superior al 80% en experimentos humanos que han medido bloqueo de todas las gotitas respiratorias, con máscaras de tela en algunos estudios que funcionan a la par con máscaras quirúrgicas como barreras para el control de las partículas que flotan en el aire.</p>
<p>Tras la creciente situación en la que nos encontramos, La Organización Mundial de la Salud y los Centros para el Control y la Prevención de Enfermedades (CDC) de EE. UU. ahora incluyen mascarillas en sus recomendaciones para frenar la propagación del virus.</p>
<p>Dichos centros recomiendan <strong> máscaras faciales de tela </strong>para el público, y no las máscaras quirúrgicas y N95 que necesitan los proveedores de atención médica.</p>
<h2>¿Qué debe tener una mascarilla de tela?</h2>
<p>El uso de mascarillas se ha convertido en la acción fundamental en la lucha contra el coronavirus, ya que están destinadas a atrapar las gotas que se liberan cuando el usuario habla, tose o estornuda.</p>
<p>Si bien las mascarillas quirúrgicas y N95 pueden escasear y deben reservarse para los proveedores de atención médica, las mascarillas faciales y las <strong> mascarillas de tela </strong>son fáciles de encontrar o fabricar, y pueden lavarse y reutilizarse.</p>
<p>Para confeccionar <strong> mascarillas de tela</strong>, se debe tener:</p>
<ul>
    <li>. Una capa interna, dedicada a absorber las partículas que choquen contra ella,</li>
    <li>. Segunda capa, o capa intermedia, que actúa como filtro entre la respiración del usuario y los agentes externos que puedan acercarse.</li>
    <li>. Una tercera capa, que esté confeccionada en poliéster.</li>
</ul>
<p>Si las <strong> mascarillas de tela</strong> cuentan con estas tres capas bien diferenciadas, será efectiva para evitar tanto la propagación como el contagio del COVID-19, al convertirse en una barrera mecánica que frena la transmisión.</p>