
<p>Las instituciones que se encargan del control y la prevención de enfermedades autorizan a las personas que van a lugares públicos cubrirse la cara con <strong>mascarillas de tela</strong>, guardando siempre el distanciamiento social y siguiendo los protocolos sanitarios básicos.
</p>

<p>El Ministerio de Salud ha aprobado el uso de mascarillas de tela comunitarias según las especificaciones MINSA y en <a href="http://confelav.pe/" target="_blank">Confelav</a> ofrecemos diversas variedades, colores y composiciones que cumplen con la normativa.</p>


<p>Las <strong>mascarillas de tela</strong> ayudan a frenar la propagación del virus ya que reducen el riesgo de exposición potencial de una persona infectada, tenga síntomas o no.
Aquellas personas que usan <strong>mascarillas de tela</strong> reducen la posibilidad de contagio o de ser contagiadas.</p>

<p>En <a href="http://confelav.pe/" target="_blank">Confelav</a> hemos desarrollado mascarillas de tela a solictud de nuestros clientes y son lavables y reutilizables. <a target="_blank" href="https://mascarillastela.pe/pdf/ficha-tecnica.pdf">Ver  ficha técnica</a></p>


<h2>¿Por qué usar mascarillas de tela?</h2>

<p>Las <strong>mascarillas de tela</strong> bien hechas participan efectivamente en el bloqueo de la mayoría de las gotas grandes (es decir, de 20 a 30 micrones y más), y también pueden bloquear la exhalación de finas gotas y partículas (también conocidas como aerosoles) de menos de 10 micrones; que aumentan en número con el volumen del habla y los tipos específicos de fonación.</p>

<p>Actualmente, existen <strong>mascarillas de tela</strong> multicapa que pueden bloquear hasta el 50-70% de estas gotas y partículas finas, y limitar la propagación hacia adelante de las que no se capturan.</p>

<p>En <a href="http://confelav.pe/" target="_blank">Confelav</a> somos conscientes que las mascarillas de tela pueden lograr un aporte importante en la reducción de la propagación del virus añadiendo el importante distanciamiento social y protocolos de higiene básicos, como el lavado de manos con jabón, gel y otros desinfectantes.</p>

<p>Tras la creciente situación en la que nos encontramos, La Organización Mundial de la Salud y los Centros para el Control y la Prevención de Enfermedades (CDC) de EEUU ahora incluyen mascarillas de tela en sus recomendaciones para frenar la propagación del virus.</p>

<p>En <a href="http://confelav.pe/" target="_blank">Confelav</a> somos conscientes que las mascarillas de tela comunitarias no cumplen las mismas funciones que las quirúrgicas y que la prevención  personal es indispensable para poder optimizar su uso y frenar la propagación del contagio.</p>

<h2>¿Qué debe tener una mascarilla de tela?</h2>

<p>El uso de mascarillas de tela se ha convertido en gran parte en la acción fundamental de lucha contra el coronavirus, ya que son accesibles y pueden utilizarse muchas veces después de lavarlas.</p>

<p>Si bien las mascarillas quirúrgicas y N95 entre otras pueden escasear y deben reservarse para los proveedores de atención médica, las mascarillas faciales y las <strong>mascarillas de tela</strong> son fáciles de encontrar o fabricar, y pueden lavarse y reutilizarse. En <a href="http://confelav.pe/" target="_blank">Confelav</a> las hacemos bien y entedemos la importancia de utilizarlas.</p>

<p>Para confeccionar <strong> mascarillas de tela</strong>, se debe tener:</p>
<ul>
    <li>• Composición de la tela </li>
    <li>• Capas protectoras</li>
    <li>• Ergonomía</li>
    <li>• Correcto colocamiento </li>
    <li>• Manual de uso</li>
    <li>• Resistencia al lavado</li>
    <li>• Otros factores</li>
</ul>



<a target="_blank" href="https://mascarillastela.pe/pdf/ficha-tecnica.pdf">Ver ficha técnica</a>

<a target="_blank" href="https://cdn.www.gob.pe/uploads/document/file/572315/RM_135-2020-MINSA.PDF">Ver disposición MINSA </a>
