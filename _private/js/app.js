"use strict";
const Confelav = (function() {  
    const data = {

    };

    const events = () => {
        $('.sala-items-header').on('click', methods.tabIntoPrendas)
    };

    const methods = {
        homePage: () => {
            if($('html').hasClass('home')){
                $('.button-secondary').each(function() {
                    let $circlesTopLeft = $(this).parent().find('.circle.top-left');
                    let $circlesBottomRight = $(this).parent().find('.circle.bottom-right');

                    let tl = new TimelineLite();
                    let tl2 = new TimelineLite();

                    let btTl = new TimelineLite({ paused: true });

                    tl.to($circlesTopLeft, 1.2, { x: -25, y: -25, scaleY: 2, ease: SlowMo.ease.config(0.1, 0.7, false) });
                    tl.to($circlesTopLeft.eq(0), 0.1, { scale: 0.2, x: '+=6', y: '-=2' });
                    tl.to($circlesTopLeft.eq(1), 0.1, { scaleX: 1, scaleY: 0.8, x: '-=10', y: '-=7' }, '-=0.1');
                    tl.to($circlesTopLeft.eq(2), 0.1, { scale: 0.2, x: '-=15', y: '+=6' }, '-=0.1');
                    tl.to($circlesTopLeft.eq(0), 1, { scale: 0, x: '-=5', y: '-=15', opacity: 0 });
                    tl.to($circlesTopLeft.eq(1), 1, { scaleX: 0.4, scaleY: 0.4, x: '-=10', y: '-=10', opacity: 0 }, '-=1');
                    tl.to($circlesTopLeft.eq(2), 1, { scale: 0, x: '-=15', y: '+=5', opacity: 0 }, '-=1');

                    let tlBt1 = new TimelineLite();
                    let tlBt2 = new TimelineLite();

                    tlBt1.set($circlesTopLeft, { x: 0, y: 0, rotation: -45 });
                    tlBt1.add(tl);

                    tl2.set($circlesBottomRight, { x: 0, y: 0 });
                    tl2.to($circlesBottomRight, 1.1, { x: 30, y: 30, ease: SlowMo.ease.config(0.1, 0.7, false) });
                    tl2.to($circlesBottomRight.eq(0), 0.1, { scale: 0.2, x: '-=6', y: '+=3' });
                    tl2.to($circlesBottomRight.eq(1), 0.1, { scale: 0.8, x: '+=7', y: '+=3' }, '-=0.1');
                    tl2.to($circlesBottomRight.eq(2), 0.1, { scale: 0.2, x: '+=15', y: '-=6' }, '-=0.2');
                    tl2.to($circlesBottomRight.eq(0), 1, { scale: 0, x: '+=5', y: '+=15', opacity: 0 });
                    tl2.to($circlesBottomRight.eq(1), 1, { scale: 0.4, x: '+=7', y: '+=7', opacity: 0 }, '-=1');
                    tl2.to($circlesBottomRight.eq(2), 1, { scale: 0, x: '+=15', y: '-=5', opacity: 0 }, '-=1');

                    tlBt2.set($circlesBottomRight, { x: 0, y: 0, rotation: 45 });
                    tlBt2.add(tl2);

                    btTl.add(tlBt1);
                    btTl.to($(this).parent().find('.button.effect-button'), 0.8, { scaleY: 1.1 }, 0.1);
                    btTl.add(tlBt2, 0.2);
                    btTl.to($(this).parent().find('.button.effect-button'), 1.8, { scale: 1, ease: Elastic.easeOut.config(1.2, 0.4) }, 1.2);

                    btTl.timeScale(2.6);

                    $(this).on('mouseover', function() {
                    btTl.restart();
                    });
                });

            }
        },  
        tabIntoPrendas: () => {

            if($(event.currentTarget).next().is(":visible")){
                $(event.currentTarget).closest(".sala-content-items").removeClass("active");
                $(event.currentTarget).next().slideUp("slow")
            }else{
                $('.sala-items-header').next().css("display", "none")
                $(".sala-content-items").removeClass("active")
                $(event.currentTarget).closest(".sala-content-items").addClass("active");
                $(event.currentTarget).next().slideDown("slow")
            }
        },
        tabPrendasMain: () => {

            let hash = window.location.hash;
            $('.my-tabs a[href="' + hash + '"]').tab('show');

            $('.my-tabs a').click(function (e) {
                $(this).tab('show');
                let scrollmem = $('body').scrollTop();
                window.location.hash = this.hash;
                $('html,body').scrollTop(scrollmem);

                e.preventDefault()
            });

        },
        carrouselHomeBanner: () => {
            let owl = $('.owl-carousel-banner');
            if(owl.length){
                owl.owlCarousel({
                    loop: true,
                    margin: 10,
                    nav: false,
                    items: 1,
                    dots: true,
                    dotsData: true,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1,

                        },
                        600: {
                            items: 1,

                        },
                    }
                })

                $('#banner-left').click(function() {
                    owl.trigger('prev.owl.carousel', [500]);
                })
                $('#banner-right').click(function() {
                    owl.trigger('next.owl.carousel', [500]);
                })
                $('.owl-dot').click(function() {
                    owl.trigger('to.owl.carousel', [$(this).index(), 1000]);
                })

            }

        },


        carouselMobile: () => {
            $('.carouselMB').flickity({
                // options
                freeScroll: true, 
                contain: true, 
                prevNextButtons: false, 
                pageDots: false
            });
        },

        gridBlog: () => {

            if($('html').hasClass('confeblog')){
                const htmlTemplate = document.getElementById("product-grid");

                fetch('_webconfig/data.php')
                .then(response => response.json())
                .then(data => {
                    let htmlContent = `${Object.keys(data).map(
                    (posts) => `
                        <div class="section-card textlinks">
                            <a href="${data[posts].post_url}">
                                <div class="card product-item ${data[posts].post_category}">
                                    <img class="img-card" src="${data[posts].post_img_short}" alt="Photo blog">
                                    <div class="text-card">
                                        <img src="_private/img/home/etiqueta.svg" width="12px" height="12px" alt="Etiqueta"><span> ${data[posts].post_category}</span>
                                        <a href="${data[posts].post_url}">
                                            <h3 class="title-card-blog">${data[posts].post_title}</h3>
                                        </a>
                                        <br>
                                        <br>
                                        <p>${data[posts].post_resumen}</p>
                                        <div class="contentFecha">
                                            <img src="_private/img/home/calendario.svg" width="12.5px" height="13px" alt="Calendario">
                                            <span>Fecha: ${data[posts].post_date}</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    `).join("")}`;
                    setTimeout(() => {
                        htmlTemplate.style.height = ""  //setea el height antes de pintar los blogs

                        htmlTemplate.innerHTML = htmlContent;

                        var gridFilter= function(){
                            if ($(".grid_sorting_button").length) {
                                $(".grid_sorting_button").click(function () {
                                $(".grid_sorting_button.active").removeClass("active");
                                $(this).addClass("active");
                    
                                var selector = $(this).attr("data-filter");
                                $(".section-card").isotope({
                                    filter: selector,
                                    animationOptions: {
                                    duration: 750,
                                    easing: "linear",
                                    queue: false,
                                    },
                                });
                    
                                return false;
                                });
                            }
                        }
                        gridFilter()

                        function setSize() { 
                            console.log('me cambiaste el tamano 2');
                            htmlTemplate.style.height = ""  //setea el height antes de pintar los blogs
                        };
                        document.getElementsByTagName("BODY")[0].onresize = function() {setSize()};
                       
                    }, 1000);    
                })

            }

        },

        carrouselHomeNews: () => {
            if($('#news').length){

                const htmlTemplate = document.querySelector.bind(document);

                fetch('_webconfig/data.php')
                .then((response) =>  response.json())
                .then((data) => {
                    let htmlContent = `${Object.keys(data).map( 
                        (posts) => `
                            <div class="section-card" >              
                                <a href="${data[posts].post_url}">
                                    <div class="card product-item ">
                                        <img class="img-card" src="${data[posts].post_img_short}" alt="Sample photo">
                                        <div class="text-card">
                                            <img src="_private/img/home/etiqueta.svg" width="12px" height="12px" alt="Etiqueta">
                                            <span> ${data[posts].post_category}</span>
                                            <a href="${data[posts].post_url}">
                                                <h3 class="title-card-blog">${data[posts].post_title}</h3>
                                            </a>
                                            <br>
                                            <br>
                                            <p>${data[posts].post_resumen}</p>
                                            <div>
                                                <img src="_private/img/home/calendario.svg" width="12.5px" height="13px" alt="Calendario">
                                                <span> Fecha: ${data[posts].post_date}</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                    `).join("")}`;

                    setTimeout(() => {
                        htmlTemplate("#news").innerHTML += '<div class="owl-carousel">  ' + htmlContent + "</div>";
                        $(".owl-carousel").owlCarousel({
                            loop: true,
                            margin: 10,
                            nav: true,
                            responsiveClass: true,
                            responsive: {
                              0: {
                                items: 1,
                              },
                              600: {
                                items: 2,
                              },
                              800: {
                                items: 3,
                              },
                            },
                        });
                    }, 1000);
                })
                .catch((error) => console.log(error));

            }
        },        
        carrouselTendencias: () => {

            if ($(".gallery-container").length) {
                const galleryContainer = document.querySelector('.gallery-container');
                const galleryControlsContainer = document.querySelector('.gallery-controls');
                const galleryControls = ['previous', 'next'];
                const galleryItems = document.querySelectorAll('.gallery-item');

                class Carousel {
                    constructor(container, items, controls) {
                        this.carouselContainer = container;
                        this.carouselControls = controls;
                        this.carouselArray = [...items];
                    }

                    // Assign initial css classes for gallery and nav items
                    setInitialState() {
                        this.carouselArray[0].classList.add('gallery-item-first');
                        this.carouselArray[1].classList.add('gallery-item-previous');
                        this.carouselArray[2].classList.add('gallery-item-selected');
                        this.carouselArray[3].classList.add('gallery-item-next');
                        this.carouselArray[4].classList.add('gallery-item-last');

                        document.querySelector('.gallery-nav').childNodes[0].className = 'gallery-nav-item gallery-item-first';
                        document.querySelector('.gallery-nav').childNodes[1].className = 'gallery-nav-item gallery-item-previous';
                        document.querySelector('.gallery-nav').childNodes[2].className = 'gallery-nav-item gallery-item-selected';
                        document.querySelector('.gallery-nav').childNodes[3].className = 'gallery-nav-item gallery-item-next';
                        document.querySelector('.gallery-nav').childNodes[4].className = 'gallery-nav-item gallery-item-last';
                    }

                    // Update the order state of the carousel with css classes
                    setCurrentState(target, selected, previous, next, first, last) {

                        selected.forEach(el => {
                            el.classList.remove('gallery-item-selected');

                            if (target.className == 'gallery-controls-previous') {
                                el.classList.add('gallery-item-next');
                            } else {
                                el.classList.add('gallery-item-previous');
                            }
                        });

                        previous.forEach(el => {
                            el.classList.remove('gallery-item-previous');

                            if (target.className == 'gallery-controls-previous') {
                                el.classList.add('gallery-item-selected');
                            } else {
                                el.classList.add('gallery-item-first');
                            }
                        });

                        next.forEach(el => {
                            el.classList.remove('gallery-item-next');

                            if (target.className == 'gallery-controls-previous') {
                                el.classList.add('gallery-item-last');
                            } else {
                                el.classList.add('gallery-item-selected');
                            }
                        });

                        first.forEach(el => {
                            el.classList.remove('gallery-item-first');

                            if (target.className == 'gallery-controls-previous') {
                                el.classList.add('gallery-item-previous');
                            } else {
                                el.classList.add('gallery-item-last');
                            }
                        });

                        last.forEach(el => {
                            el.classList.remove('gallery-item-last');

                            if (target.className == 'gallery-controls-previous') {
                                el.classList.add('gallery-item-first');
                            } else {
                                el.classList.add('gallery-item-next');
                            }
                        });
                    }

                    // Construct the carousel navigation
                    setNav() {
                        galleryContainer.appendChild(document.createElement('ul')).className = 'gallery-nav';

                        this.carouselArray.forEach(item => {
                            const nav = galleryContainer.lastElementChild;
                            nav.appendChild(document.createElement('li'));
                        });
                    }

                    // Construct the carousel controls
                    setControls() {
                        this.carouselControls.forEach(control => {
                            galleryControlsContainer.appendChild(document.createElement('button')).className = `gallery-controls-${control}`;
                        });

                        /* !!galleryControlsContainer.childNodes[0] ? galleryControlsContainer.childNodes[0].innerHTML = this.carouselControls[0] : null;
                        !!galleryControlsContainer.childNodes[1] ? galleryControlsContainer.childNodes[1].innerHTML = this.carouselControls[1] : null; */
                    }

                    // Add a click event listener to trigger setCurrentState method to rearrange carousel
                    useControls() {
                        const triggers = [...galleryControlsContainer.childNodes];

                        triggers.forEach(control => {
                            control.addEventListener('click', () => {
                                const target = control;
                                const selectedItem = document.querySelectorAll('.gallery-item-selected');
                                const previousSelectedItem = document.querySelectorAll('.gallery-item-previous');
                                const nextSelectedItem = document.querySelectorAll('.gallery-item-next');
                                const firstCarouselItem = document.querySelectorAll('.gallery-item-first');
                                const lastCarouselItem = document.querySelectorAll('.gallery-item-last');

                                this.setCurrentState(target, selectedItem, previousSelectedItem, nextSelectedItem, firstCarouselItem, lastCarouselItem);
                            });
                        });
                    }
                }

                const carousel = new Carousel(galleryContainer, galleryItems, galleryControls);

                carousel.setControls();
                carousel.setNav();
                carousel.setInitialState();
                carousel.useControls();

                document.querySelector('ul.gallery-nav').className = 'd-none';


            

                $('.gallery').each(function() {
                    $('body').on("click", ".gallery-item", function() {
                        let galleryItem = $(this);
                        let modal = $("#modal");

                        modal.find('.modal-tendencias-img').attr("src", galleryItem.find('img').attr("src"));
                        modal.find('.modal-tendencias-title').text(galleryItem.find('.product-name').val());
                        modal.find('.modal-tendencias-desc').text(galleryItem.find('.product-description').val());
                        $('#modal').modal('show')
                    });
                });

            }
        },

        getactivelink: () => {
            $('.header-options-ul .header-options-link').click(function(e) {
                
                
                $('.header-options-link').removeClass('active');
                $(this).addClass('active');
            });
        },
        
        animatescrolltopindex: ()=> {
            $('.header-menu-li').on('click', '.header-options-link', function(){
                
                const id= $(this).attr('href')
               
                $('html, body').animate({
                    scrollTop: $(id).offset().top
                }, 1000)
              })
        },
        firsthashurl:()=>{
            $(document).ready(function() {
                const idfirstload=  window.location.hash;            
                $('.header-options-ul .header-options-link'+idfirstload+'act').addClass('active');       
            })    
        }
    };
   

    const initialize = () => {
        events();

        methods.homePage();
        methods.tabPrendasMain();
        methods.carrouselHomeBanner();
        methods.carrouselTendencias();
        methods.carrouselHomeNews();
        methods.gridBlog();
        methods.getactivelink();
        methods.animatescrolltopindex();
        methods.firsthashurl();
        methods.carouselMobile();
    };

    return {
        init: initialize
    };
})();


document.addEventListener(
    'DOMContentLoaded',
    () => {
        Confelav.init();
        AOS.init();
    },
    false
);