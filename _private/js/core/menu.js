/**
 * Home
 * @page Core
 * @author mfyance
 */
"use strict"

const html = document.getElementsByTagName('html')[0];
const btn = document.querySelector('[menu-button]');
const option = document.querySelectorAll('[menu-li]');

btn.addEventListener('click', evt => {
  	if ( html.hasAttribute("menu-active") ){
		html.removeAttribute("menu-active")
  	} else {
		html.setAttribute("menu-active","")
		
  	}
})

option.forEach( el => {    	
	el.addEventListener('click', evt => {   			
		html.removeAttribute("menu-active")
	})
})



