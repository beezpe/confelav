<?php 
    require '../../_webconfig/root.php';
    $page_title = "Confelav 🏅 Confecciones de Lavandería";
?>

<!DOCTYPE html>

<html lang="es">

<head>
    <?php include '../../_partials/head.php' ?>
</head>

<body>
    <!-- Header -->
    <?php include '../../_partials/header.php';?>
    <!-- /Header -->

    <main class="main-blog">

       

        <contenedor layout-max class="richtext">

            <div class="content-Details">

                <?php include "../../_private/articulos/2020/importancia-mantener-limpio-uniforme/articulo.php" ?>  
                
            </div>          

        </contenedor>



        <div class="content-call" data-animation="animated zoomIn">
            <a target="_blank" href="https://wa.me/+51962927662"  class="button button-first" data-tagmanager="articulo-2020-eleccion-tela">
                <img src="./_private/img/icons/telefono.svg" alt="icon" class="button-icon"> 
                Conversemos ahora
            </a>   
        </div>

       
    </main>

     <!-- Footer -->
     <?php include '../../_partials/footer.php';?>
    <!-- /Footer -->

    <!-- Scripts -->
    <?php include '../../_partials/scripts.php';?>
    <!-- /Scripts -->
</body>


</html>