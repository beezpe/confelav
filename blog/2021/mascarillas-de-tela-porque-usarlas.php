<?php 
    require '../../_webconfig/root.php';

    $cur_dir = explode('\\', getcwd());
    $blog_year =  $cur_dir[count($cur_dir)-1];
    $blog_file = basename($_SERVER['PHP_SELF']);
    $blog_name = pathinfo($blog_file, PATHINFO_FILENAME);
    $blog_img = '_articles/'.$blog_year.'/'.$blog_name.'/large.jpg';
    $blog_site = '../../_articles/'.$blog_year.'/'.$blog_name.'/articulo.php';
    $blog_date = date("d/m/Y", filectime($blog_file));


    $blog_title = "Mascarillas de tela. ¿Por qué usarlas?";
    $page_title = "Confelav 🏅 ".$blog_title;
?>

<!DOCTYPE html>

<html lang="es">

<head>
    <?php include '../../_partials/head.php' ?>
</head>

<body>
    <!-- Header -->
    <?php include '../../_partials/header.php';?>
    <!-- /Header -->

    <main class="main-blog">

       

        <contenedor layout-max class="richtext">

            <div class="content-Details">

                <blog-banner style="background-image:url(<?php echo $blog_img; ?>);"></blog-banner>
                <h1><?php echo $blog_title ?></h1>
                <blog-date><img src="_private/img/icons/calendar.svg">Fecha: <?php echo $blog_date ?></blog-date>

                <?php include $blog_site; ?>

            </div>           

        </contenedor>

       
        <div class="content-call" data-animation="animated zoomIn">
            <a target="_blank" href="https://wa.me/+51962927662"  class="button button-first" data-tagmanager="articulo-2020-eleccion-tela">
                <img src="./_private/img/icons/telefono.svg" alt="icon" class="button-icon"> 
                Conversemos ahora  
            </a>   
        </div>

       
    </main>

     <!-- Footer -->
     <?php include '../../_partials/footer.php';?>
    <!-- /Footer -->

    <!-- Scripts -->
    <?php include '../../_partials/scripts.php';?>
    <!-- /Scripts -->
</body>


</html>